// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PurchasableUnitWidget.generated.h"

/**
 * 
 */
UCLASS()
class VIKTORIA_SHT_GB_API UPurchasableUnitWidget : public UUserWidget
{
	GENERATED_BODY()
	
};
