// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Viktoria_Sht_GBGameMode.generated.h"

UCLASS(minimalapi)
class AViktoria_Sht_GBGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AViktoria_Sht_GBGameMode();
};



