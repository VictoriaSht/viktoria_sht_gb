// Fill out your copyright notice in the Description page of Project Settings.


#include "InterfaceForPlayerHUD.h"

// Add default functionality here for any IInterfaceForPlayerHUD functions that are not pure virtual.

void IInterfaceForPlayerHUD::UpdateMoney(int32 Income, int32 BonusIncome, bool bPositive)
{
}

void IInterfaceForPlayerHUD::OpenPoweringUpMenu(int32 PowerUpCost, AActor* object)
{
}

void IInterfaceForPlayerHUD::ClosePoweringUpMenu()
{
}

void IInterfaceForPlayerHUD::HandleMovement(bool bMove)
{
}

void IInterfaceForPlayerHUD::MessageCantPowerUp()
{
}

int32 IInterfaceForPlayerHUD::GetMoney()
{
	return int32();
}

