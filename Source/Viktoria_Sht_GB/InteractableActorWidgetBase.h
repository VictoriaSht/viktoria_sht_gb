// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InteractableActorWidgetBase.generated.h"

/**
 * 
 */
UCLASS()
class VIKTORIA_SHT_GB_API UInteractableActorWidgetBase : public UUserWidget
{
	GENERATED_BODY()
	
public:

	//Setting up new values
	void SetMoney(int32 CurrentMoney);
	void SetNameDesc(FString _Name, FText _Description);
	void UpdatePowerUpCost(int32 PowerUpCost);
	void CantPowerUp();


	//Widget components

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* Name = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* Description = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* PowerUpCostText = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	class UBorder* PowerUpTipBorder = nullptr;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UTextBlock* ObjectMoneyText = nullptr;


	//UPROPERTY(EditAnywhere, meta = (BindWidget))
	//class UWrapBox* NameDesc = nullptr;

	/*
	Events for work and name | description, overrided in blueprints
	*/

	UFUNCTION(BlueprintNativeEvent)
	void StartDoingWork();
	void StartDoingWork_Implementation();

	UFUNCTION(BlueprintNativeEvent)
	void EndDoingWork();
	void EndDoingWork_Implementation();

	UFUNCTION(BlueprintNativeEvent)
	void MakeNameDescVisible();
	void MakeNameDescVisible_Implementation();

	UFUNCTION(BlueprintNativeEvent)
	void MakeNameDescNotVisible();
	void MakeNameDescNotVisible_Implementation();

	/**/

};
