// Copyright Epic Games, Inc. All Rights Reserved.

#include "Viktoria_Sht_GBGameMode.h"
#include "Viktoria_Sht_GBPlayerController.h"
#include "Viktoria_Sht_GBCharacter.h"
#include "UObject/ConstructorHelpers.h"

AViktoria_Sht_GBGameMode::AViktoria_Sht_GBGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AViktoria_Sht_GBPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}