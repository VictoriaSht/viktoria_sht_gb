﻿// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "InterfaceForPlayerHUD.h"
#include "InteractableActor.h"
#include "Viktoria_Sht_GBCharacter.generated.h"

UCLASS(Blueprintable)
class AViktoria_Sht_GBCharacter : public ACharacter, public IInterfaceForPlayerHUD
{
	GENERATED_BODY()

public:
	AViktoria_Sht_GBCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;


public:
	/*
	Overlap handling
	*/

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComponent,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);

	UFUNCTION()
	void HandleEndOverlap(UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComponent,
		int32 OtherBodyIndex);

	/*
	*HUD
	*/

	/* Setup HUD*/
	virtual void BeginPlay() override;

	/*Clear HUD*/
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	/*Widget class to spawn heads up display*/
	UPROPERTY(EditAnywhere)
	TSubclassOf<class UPlayerHUD> PlayerHUDClass;

	/*The widget instanse*/
	UPROPERTY()
	class UPlayerHUD* PlayerHUD;

	//Players money
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 PlayerMoney;

	/*
	From interface
	*/
	virtual void UpdateMoney(int32 Income, int32 BonusIncome = 0, bool bPositive = true) override;
	virtual void OpenPoweringUpMenu(int32 PowerUpCost, AActor* object) override;
	virtual void ClosePoweringUpMenu();
	virtual void HandleMovement(bool bMove) override;
	virtual void MessageCantPowerUp() override;
	virtual int32 GetMoney() override;
	/**/
};

