// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Viktoria_Sht_GB : ModuleRules
{
	public Viktoria_Sht_GB(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "NavigationSystem", "AIModule", "Niagara", "EnhancedInput" });
    }
}
