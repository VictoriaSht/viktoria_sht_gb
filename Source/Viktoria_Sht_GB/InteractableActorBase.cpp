// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractableActorBase.h"

// Sets default values
AInteractableActorBase::AInteractableActorBase()
{
	//Create a sphere collision component
	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision->SetSphereRadius(100);

	//Create widget component
	StatsWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("StatsWidget"));
	StatsWidget->AttachToComponent(SphereCollision, FAttachmentTransformRules::KeepRelativeTransform);
	StatsWidget->SetVisibility(true, true);
	StatsWidget->SetWidgetSpace(EWidgetSpace::World);

}

// Called when the game starts or when spawned
void AInteractableActorBase::BeginPlay()
{
	Super::BeginPlay();
	
	//Pass current money value to the Widget
	WidgetInstance = Cast<UInteractableActorWidgetBase>(StatsWidget->GetUserWidgetObject());
	WidgetInstance->SetNameDesc(Name, Descritpion);
}

// Called every frame
void AInteractableActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void AInteractableActorBase::ShowNameDesc()
{
	WidgetInstance->MakeNameDescVisible();
}

void AInteractableActorBase::HideNameDesc()
{
	WidgetInstance->MakeNameDescNotVisible();
}


