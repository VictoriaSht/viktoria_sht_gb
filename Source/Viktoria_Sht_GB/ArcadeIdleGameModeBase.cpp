// Fill out your copyright notice in the Description page of Project Settings.


#include "ArcadeIdleGameModeBase.h"
#include "Viktoria_Sht_GBPlayerController.h"
#include "Viktoria_Sht_GBCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "InteractableActor.h"
#include "Kismet/GameplayStatics.h"

AArcadeIdleGameModeBase::AArcadeIdleGameModeBase()
{
	// use our custom PlayerController class
	PlayerControllerClass = AViktoria_Sht_GBPlayerController::StaticClass();


	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ArcadeIdle/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/ArcadeIdle/Blueprints/BP_TopDownPlayerController"));
	if (PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}

void AArcadeIdleGameModeBase::LoadGame()
{
}

void AArcadeIdleGameModeBase::SaveGame()
{
	//MySaveGame->
}
