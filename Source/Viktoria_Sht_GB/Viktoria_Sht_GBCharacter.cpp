// Copyright Epic Games, Inc. All Rights Reserved.

#include "Viktoria_Sht_GBCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Interact.h"
#include "PlayerHUD.h"
#include "Blueprint/UserWidget.h"
#include "Viktoria_Sht_GBPlayerController.h"

#include "Components/Button.h"

AViktoria_Sht_GBCharacter::AViktoria_Sht_GBCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	//Bind function to overlap event 
	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &AViktoria_Sht_GBCharacter::HandleBeginOverlap);
	GetCapsuleComponent()->OnComponentEndOverlap.AddDynamic(this, &AViktoria_Sht_GBCharacter::HandleEndOverlap);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	// HUD
	PlayerHUDClass = nullptr;
	PlayerHUD = nullptr;

	//Money
	PlayerMoney = 0;
}

void AViktoria_Sht_GBCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (IsLocallyControlled() && PlayerHUDClass)
	{
		//Creating a HUD
		AViktoria_Sht_GBPlayerController* PlayerController = GetController<AViktoria_Sht_GBPlayerController>();
		check(PlayerController);
		PlayerHUD = CreateWidget<UPlayerHUD>(PlayerController, PlayerHUDClass);
		check(PlayerHUD);
		PlayerHUD->AddToPlayerScreen();

		//Pass current money value to the HUD
		PlayerHUD->SetMoney(PlayerMoney);
	}
}

void AViktoria_Sht_GBCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (PlayerHUD)
	{
		//Clearing everything
		PlayerHUD->RemoveFromParent();
		PlayerHUD = nullptr;
	}

	Super::EndPlay(EndPlayReason);
}

/*functions from INTERFACE FOR PLAYERHUD*/

void AViktoria_Sht_GBCharacter::UpdateMoney(int32 Income, int32 BonusIncome, bool bPositive)
{
	if (bPositive) PlayerMoney += Income + BonusIncome;
	else PlayerMoney -= Income;

	PlayerHUD->SetMoney(PlayerMoney);
	PlayerHUD->AddIncomeNotification(Income, BonusIncome, bPositive);
}

int32 AViktoria_Sht_GBCharacter::GetMoney()
{
	return PlayerMoney;
}

void AViktoria_Sht_GBCharacter::OpenPoweringUpMenu(int32 PowerUpCost, AActor* object)
{
	PlayerHUD->OpenPoweringUpMenu(PowerUpCost);

	//Bind YesButton to objects PowerUp so we dont need to pass object reference to widget
	IInteract* InteractiveActorRef = Cast<IInteract>(object);
	PlayerHUD->YesButton->OnClicked.AddUniqueDynamic(InteractiveActorRef, &IInteract::PowerUp);
}

void AViktoria_Sht_GBCharacter::ClosePoweringUpMenu()
{
	PlayerHUD->ClosePoweringUpMenu();
}

void AViktoria_Sht_GBCharacter::HandleMovement(bool bMove)
{
	GetController()->SetIgnoreLookInput(bMove);
	GetController()->SetIgnoreMoveInput(bMove);
}

void AViktoria_Sht_GBCharacter::MessageCantPowerUp()
{
		PlayerHUD->MessageYouDontHaveMoney();
}


void AViktoria_Sht_GBCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
}

/*INTERACT*/

void AViktoria_Sht_GBCharacter::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, 
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComponent, 
	int32 OtherBodyIndex, 
	bool bFromSweep, 
	const FHitResult& SweepResult)
{
	IInteract* InteractInterface = Cast<IInteract>(OtherActor);
	if (InteractInterface)
	{
		InteractInterface->Interact(this);
	}
}

void AViktoria_Sht_GBCharacter::HandleEndOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComponent,
	int32 OtherBodyIndex)
{
	IInteract* InteractInterface = Cast<IInteract>(OtherActor);
	if (InteractInterface)
	{
		InteractInterface->EndInteract(this);
	}
}


