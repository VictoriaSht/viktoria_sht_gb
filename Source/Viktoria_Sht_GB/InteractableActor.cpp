﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractableActor.h"
#include "PlayerHUD.h" //for interact
#include "Blueprint/UserWidget.h"
#include "InteractableActorWidgetBase.h"

// Sets default values
AInteractableActor::AInteractableActor()
{
	//Sphere collision and WidgetStats are initialized in AInteractableActorBase()

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Initialising IncomeValue and PowerUpCost
	IncomeValue.Init(1, 4);
	PowerUpCost.Init(1, 3);

	//Ticks will be 2 seconds long
	IncomeInterval = 2;
}

// Called when the game starts or when spawned
void AInteractableActor::BeginPlay()
{
	Super::BeginPlay();

	//WidgetInstance is initialized in AInteractableActorBase()

	//Setting how fast the money will store
	SetActorTickInterval(IncomeInterval);

	//Pass current money value and powerupTip to the Widget
	WidgetInstance->SetMoney(CurrentCash);
	WidgetInstance->UpdatePowerUpCost(PowerUpCost[PowerIndex]);

	//Bind StartProblemSolving to Finished animation
	//EndDelegate.BindDynamic(this, &AInteractableActor::StartProblemSolving);
	//WidgetInstance->BindToAnimationFinished(WidgetInstance->Working, EndDelegate);
}

// Called every frame
void AInteractableActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Check if object have problem
	if (!bHasProblem)
	{
		//If player is not working, stack money, otherwise pass money to player
		if (!bIsWorking)
		{
			CurrentCash += IncomeValue[PowerIndex];
			WidgetInstance->SetMoney(CurrentCash);
		}
		else if (PlayerRef)
		{
			PlayerRef->UpdateMoney(IncomeValue[PowerIndex], WorkIncomeBonus);
		}
	}

}

//Binds to "YesButton" in PlayerHUD (PoweingUpMenu)
void AInteractableActor::PowerUp()
{
	if (PlayerRef)
	{
		//Enable movement and close "Powering up menu"
		PlayerRef->HandleMovement(false);
		PlayerRef->ClosePoweringUpMenu();

		//Power Up and take money
		PlayerRef->UpdateMoney(PowerUpCost[PowerIndex], 0, false);
		PowerIndex++;

		//Change PowerUpTip under the description in widget
		if (PowerIndex < PowerUpCost.Num())
		{
			WidgetInstance->UpdatePowerUpCost(PowerUpCost[PowerIndex]);
		}
		else WidgetInstance->CantPowerUp();

		//Call function (overrided in blueprints)
		PoweredUp();
	}
}

void AInteractableActor::InvestMoney(IInterfaceForPlayerHUD* Investor)
{
	//Check if we can Power Up
	if (PowerIndex < (PowerUpCost.Num()))
	{
		//Check if player have enough money
		if (Investor->GetMoney() >= PowerUpCost[PowerIndex])
		{
			//Memorizing Player for future power up
			PlayerRef = Investor;

			//Opening "poweirng up menu" in PlayerHUD
			int32 PowerUpCurrentCost = PowerUpCost[PowerIndex];
			Investor->OpenPoweringUpMenu(PowerUpCurrentCost, this);

			//Make player to stop movement
			Investor->HandleMovement(true);
		}
		else Investor->MessageCantPowerUp(); //Message dont have money
	}
}

void AInteractableActor::ProblemArisen()
{
	bHasProblem = true;

	//If player is working, start Solving, else Solving will start after player start working (in interact)
	if (bIsWorking)
	{
		StartProblemSolving();
	}

}

void AInteractableActor::ProblemCleared()
{
	bHasProblem = false;
}


void AInteractableActor::PoweredUp_Implementation()
{
}

void AInteractableActor::StartProblemSolving_Implementation()
{
}

void AInteractableActor::EndProblemSolving_Implementation()
{
}

//Function from interface "Interact"
void AInteractableActor::Interact(AActor* Interactor)
{
	//Memorizing player for future making work in tick
	PlayerRef = Cast<IInterfaceForPlayerHUD>(Interactor);

	//Pass money to player and reset object's stacked money
	if (CurrentCash != 0)
	{
		PlayerRef->UpdateMoney(CurrentCash);
		CurrentCash = 0;
		WidgetInstance->SetMoney(CurrentCash);
	}

	//Firing event in widget (overrided in blueprints)
	if (!bIsWorking) 
	{ 
		WidgetInstance->StartDoingWork(); 
		StartProblemSolving();
		bIsWorking = true;
	}
}

void AInteractableActor::EndInteract(AActor* Interactor)
{
	//Firing event in widget (overrided in blueprints)
	WidgetInstance->EndDoingWork();

	//If player didn't finish solving issue, timer clears
	GetWorldTimerManager().ClearTimer(IssueSolvingTimerHandle);

	PlayerRef = nullptr;
	bIsWorking = false;
}





