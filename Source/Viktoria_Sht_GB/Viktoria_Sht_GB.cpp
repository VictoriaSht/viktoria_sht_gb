// Copyright Epic Games, Inc. All Rights Reserved.

#include "Viktoria_Sht_GB.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Viktoria_Sht_GB, "Viktoria_Sht_GB" );

DEFINE_LOG_CATEGORY(LogViktoria_Sht_GB)
 