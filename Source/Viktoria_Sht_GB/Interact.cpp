// Fill out your copyright notice in the Description page of Project Settings.


#include "Interact.h"

// Add default functionality here for any IInteract functions that are not pure virtual.

void IInteract::Interact(AActor* Interactior)
{
}

void IInteract::EndInteract(AActor* Interactior)
{
}

void IInteract::ShowNameDesc()
{
}

void IInteract::HideNameDesc()
{
}

void IInteract::InvestMoney(IInterfaceForPlayerHUD* Investor)
{
}

void IInteract::PowerUp()
{
}

