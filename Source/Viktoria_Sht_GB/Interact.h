// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InterfaceForPlayerHUD.h"
#include "Interact.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInteract : public UInterface
{
	GENERATED_BODY()
};

class VIKTORIA_SHT_GB_API IInteract
{
	GENERATED_BODY()

public:

	virtual void Interact(AActor* Interactior);
	virtual void EndInteract(AActor* Interactior);
	virtual void ShowNameDesc();
	virtual void HideNameDesc();
	virtual void InvestMoney(IInterfaceForPlayerHUD* Investor);
	virtual void PowerUp();
};
