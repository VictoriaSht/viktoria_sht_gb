// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InterfaceForPlayerHUD.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInterfaceForPlayerHUD : public UInterface
{
	GENERATED_BODY()

};

class VIKTORIA_SHT_GB_API IInterfaceForPlayerHUD
{
	GENERATED_BODY()
public:

	virtual void UpdateMoney(int32 Income, int32 BonusIncome = 0, bool bPositive = true);
	virtual void OpenPoweringUpMenu(int32 PowerUpCost, AActor* object);
	virtual void ClosePoweringUpMenu();
	virtual void HandleMovement(bool bMove);
	virtual void MessageCantPowerUp();
	virtual int32 GetMoney();
};
