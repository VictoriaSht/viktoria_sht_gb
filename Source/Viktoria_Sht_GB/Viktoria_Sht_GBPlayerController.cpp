// Copyright Epic Games, Inc. All Rights Reserved.

#include "Viktoria_Sht_GBPlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "Viktoria_Sht_GBCharacter.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "InputActionValue.h"
#include "EnhancedInputSubsystems.h"
#include "Engine/LocalPlayer.h"
#include "InteractableActor.h"
#include "InterfaceForPlayerHUD.h"

DEFINE_LOG_CATEGORY(LogTemplateCharacter);

AViktoria_Sht_GBPlayerController::AViktoria_Sht_GBPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	CachedDestination = FVector::ZeroVector;
	FollowTime = 0.f;
	InteractInterface = nullptr;
}

void AViktoria_Sht_GBPlayerController::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}
}

void AViktoria_Sht_GBPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	if (bIsTouch)
	{
		bTickHitSuccessful = GetHitResultUnderFinger(ETouchIndex::Touch1, ECollisionChannel::ECC_Visibility, true, TickHit);
	}
	else
	{
		bTickHitSuccessful = GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, TickHit);
	}

	// If we hit a surface, cache the location
	if (bTickHitSuccessful)
	{
		//If we are hitting interactable actor + name/desc NOT opened -> open name/desc
		//If we are NOT hitting interactable actor + name/desc opened -> close name/desc
		InteractInterface = Cast<IInteract>(TickHit.GetActor());
		if (InteractInterface)
		{
			if (!bActorNameDescOpened)
			{
				InteractInterface->ShowNameDesc();
				bActorNameDescOpened = true;
				InteractInterfaceMemo = InteractInterface;
			}
		}
		else if (bActorNameDescOpened)
		{
			InteractInterfaceMemo->HideNameDesc();
			bActorNameDescOpened = false;
			InteractInterfaceMemo = nullptr;

		}
	}

}

void AViktoria_Sht_GBPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent))
	{
		// Setup mouse input events
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Started, this, &AViktoria_Sht_GBPlayerController::OnInputStarted);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Triggered, this, &AViktoria_Sht_GBPlayerController::OnSetDestinationTriggered);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Completed, this, &AViktoria_Sht_GBPlayerController::OnSetDestinationReleased);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Canceled, this, &AViktoria_Sht_GBPlayerController::OnSetDestinationReleased);

		// Setup touch input events
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Started, this, &AViktoria_Sht_GBPlayerController::OnInputStarted);
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Triggered, this, &AViktoria_Sht_GBPlayerController::OnTouchTriggered);
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Completed, this, &AViktoria_Sht_GBPlayerController::OnTouchReleased);
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Canceled, this, &AViktoria_Sht_GBPlayerController::OnTouchReleased);
	}
	else
	{
		UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input Component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}

void AViktoria_Sht_GBPlayerController::OnInputStarted()
{
	StopMovement();
}

// Triggered every frame when the input is held down
void AViktoria_Sht_GBPlayerController::OnSetDestinationTriggered()
{
	// We flag that the input is being pressed
	FollowTime += GetWorld()->GetDeltaSeconds();
	
		// We look for the location in the world where the player has pressed the input
		FHitResult Hit;
		bool bHitSuccessful = false;
		if (bIsTouch)
		{
			bHitSuccessful = GetHitResultUnderFinger(ETouchIndex::Touch1, ECollisionChannel::ECC_Visibility, true, Hit);
		}
		else
		{
			bHitSuccessful = GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, Hit);
		}

		// If we hit a surface, cache the location
		if (bHitSuccessful)
		{
			CachedDestination = Hit.Location;
		}

		// Move towards mouse pointer or touch
		APawn* ControlledPawn = GetPawn();
		if (ControlledPawn != nullptr)
		{
			FVector WorldDirection = (CachedDestination - ControlledPawn->GetActorLocation()).GetSafeNormal();
			ControlledPawn->AddMovementInput(WorldDirection, 1.0, false);
		}
}

void AViktoria_Sht_GBPlayerController::OnSetDestinationReleased()
{
	// If it was a short press
	if (FollowTime <= ShortPressThreshold )
	{
		//Decide if we move or invest money
		if (bActorNameDescOpened)
		{
			IInterfaceForPlayerHUD* ControlledChar = Cast<IInterfaceForPlayerHUD>(GetCharacter());
			InteractInterfaceMemo->InvestMoney(ControlledChar);
		}
		else
		{
			// We move there and spawn some particles
			UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, CachedDestination);
			UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, CachedDestination, FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
		}
	}

	FollowTime = 0.f;
}

// Triggered every frame when the input is held down
void AViktoria_Sht_GBPlayerController::OnTouchTriggered()
{
	bIsTouch = true;
	OnSetDestinationTriggered();
}

void AViktoria_Sht_GBPlayerController::OnTouchReleased()
{
	bIsTouch = false;
	OnSetDestinationReleased();
}
