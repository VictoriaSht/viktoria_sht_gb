// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interact.h"
#include "Components/SphereComponent.h"
#include "Components/WidgetComponent.h"
#include "InteractableActorWidgetBase.h"
#include "InteractableActorBase.generated.h"

UCLASS()
class VIKTORIA_SHT_GB_API AInteractableActorBase : public AActor, public IInteract
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractableActorBase();

	//Collision sphere
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	USphereComponent* SphereCollision;

	//InteractableActor Widget for stats
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UWidgetComponent* StatsWidget;
	class UInteractableActorWidgetBase* WidgetInstance;

	/*
	* Blueptint Editable Defaults
	*/

	//Object's name (is visible when player's cursor is on object)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Name = FString(TEXT("Object's name"));

	//Object's description (is visible when player's cursor is on object)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Descritpion = FText(FText::FromString(TEXT("Object's description")));

	/**/

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void ShowNameDesc() override;
	virtual void HideNameDesc() override;
};
