﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerHUD.h"
#include "Components/TextBlock.h"
#include "Components/Border.h"
#include "Components/Button.h"
#include "Components/VerticalBox.h"

bool UPlayerHUD::Initialize()
{
	bool success = Super::Initialize();
	if (!success)  return false;

	if (!ensure(NoButton)) { return false; }
	NoButton->OnClicked.AddDynamic(this, &UPlayerHUD::NoButtonClicked);

	return true;
}

//Setting up new money value in PlayerMoneyText
void UPlayerHUD::SetMoney(int32 CurrentMoney)
{
	if (PlayerMoneyText)
	{
		PlayerMoneyText->SetText(FText::AsNumber(CurrentMoney));
	}
}

void UPlayerHUD::AddIncomeNotification_Implementation(int32 Income, int32 BonusIncome, bool bPositive)
{
}

void UPlayerHUD::OpenPoweringUpMenu(int32 PowerUpCost)
{
	Cost->SetText(FText::FromString(FString::FromInt(PowerUpCost)));
	PoweringUp->SetVisibility(ESlateVisibility::Visible);
	//YesButton.OnClicked binded in character func "OpenPoweringUpMenu";
}

void UPlayerHUD::ClosePoweringUpMenu()
{
	PoweringUp->SetVisibility(ESlateVisibility::Collapsed);
	YesButton->OnClicked.Clear();
}

void UPlayerHUD::NoButtonClicked()
{
	GetWorld()->GetFirstPlayerController()->SetIgnoreLookInput(false);
	GetWorld()->GetFirstPlayerController()->SetIgnoreMoveInput(false);
	ClosePoweringUpMenu();
}

void UPlayerHUD::MessageYouDontHaveMoney_Implementation()
{
}
