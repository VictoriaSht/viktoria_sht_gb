// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InteractableActor.h"
#include "InterfaceForPlayerHUD.h"
#include "PlayerHUD.generated.h"

/**
 * 
 */
UCLASS()
class VIKTORIA_SHT_GB_API UPlayerHUD : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual bool Initialize() override;

public:
	//Setting up new money value in PlayerMoneyText
	void SetMoney(int32 CurrentMoney);

	UFUNCTION()
	void NoButtonClicked();

	//TextBlock money count
	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UTextBlock* PlayerMoneyText = nullptr;

	//Canvas panel for Powering Up
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	class UVerticalBox* IncomeNotifications = nullptr;

	//Canvas panel for Powering Up
	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UBorder* PoweringUp = nullptr;


	/*Variables for Powering up menu*/

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UButton* YesButton = nullptr;
	
	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UButton* NoButton = nullptr;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UTextBlock* Cost = nullptr;

	/**/

	//Alert why you cant power up object
	UFUNCTION(BlueprintNativeEvent)
	void MessageYouDontHaveMoney();
	void MessageYouDontHaveMoney_Implementation();


	//Money income notifications
	UFUNCTION(BlueprintNativeEvent)
	void AddIncomeNotification(int32 Income, int32 BonusIncome = 0, bool bPositive = true);
	void AddIncomeNotification_Implementation(int32 Income, int32 BonusIncome = 0, bool bPositive = true);


	//Functions from interface "InterfaceForPlayerHUD"
	void OpenPoweringUpMenu(int32 PowerUpCost);
	void ClosePoweringUpMenu();
	
};
