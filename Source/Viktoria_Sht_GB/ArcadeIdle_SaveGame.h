// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "ArcadeIdle_SaveGame.generated.h"

/**
 * 
 */
UCLASS()
class VIKTORIA_SHT_GB_API UArcadeIdle_SaveGame : public USaveGame
{
	GENERATED_BODY()

public:

	UPROPERTY(VisibleAnywhere, Category = "Saved player data")
	int32 PlayerMoney = 0;
	
};