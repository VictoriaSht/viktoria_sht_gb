// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ArcadeIdle_SaveGame.h"
#include "ArcadeIdleGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class VIKTORIA_SHT_GB_API AArcadeIdleGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AArcadeIdleGameModeBase();

	/*
	Save and Load Game
	*/
	void LoadGame();
	void SaveGame();

private:
	//AArcadeIdle_SaveGame* MySaveGame;
};
