﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractableActorBase.h"
#include "InterfaceForPlayerHUD.h"
#include "InteractableActor.generated.h"

UCLASS()
class VIKTORIA_SHT_GB_API AInteractableActor : public AInteractableActorBase
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractableActor();

	/*
	* Blueptint Editable Defaults
	*/

	//How fast the money will store
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 IncomeInterval;

	//1, 2, 3 etc - power up levels
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 PowerIndex;

	//How much extra money player get for working per one Income itteration
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 WorkIncomeBonus = 4;


	//How much money per tick (on every power state)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<int32> IncomeValue;

	//How much money does Power Up cost
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<int32> PowerUpCost;

	/**/


	//How much money stores right now
	//UPROPERTY(BlueprintReadWrite)
	int32 CurrentCash;

	//To check if we are working
	bool bIsWorking = false;

	//Cashed player (with use of interface)
	IInterfaceForPlayerHUD* PlayerRef;

private:
	FWidgetAnimationDynamicEvent EndDelegate;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	//Binds to YesButton in PlayerHUD (Powering Up menu)
	UFUNCTION()
	virtual void PowerUp() override;

	//Overrides in Blueprint so we can change mesh etc
	UFUNCTION(BlueprintNativeEvent)
	void PoweredUp();
	void PoweredUp_Implementation();

	/*
	For "Problems" logic
	*/
	UPROPERTY(BlueprintReadOnly)
	bool bHasProblem = false;

	UFUNCTION(BlueprintNativeEvent)
	void StartProblemSolving();
	void StartProblemSolving_Implementation();

	UFUNCTION(BlueprintNativeEvent)
	void EndProblemSolving();
	void EndProblemSolving_Implementation();

	UPROPERTY(BlueprintReadWrite)
	float HowFrequentProblemsAppear = 20;

	UPROPERTY(BlueprintReadWrite)
	float HowFastPoblemsSolve = 1;

	UPROPERTY(BlueprintReadWrite)
	int32 IssueIndex = 0;

	UPROPERTY(BlueprintReadWrite)
	FTimerHandle IssueSolvingTimerHandle;

	/**/

	//Functions from interface "Interact"
	virtual void Interact(AActor* Interactor) override;
	virtual void EndInteract(AActor* Interactor) override;
	virtual void InvestMoney(IInterfaceForPlayerHUD* Investor) override;
	
	//Show and Hide NameDesc are in AInteractableActorBase

	/*
	Problems logic
	*/
	//Are blueprint events so we can make custom blueprint childActors with blueprint logic
	//Game designer may be wanting to do something special for their "problems"
	//c++ code doesnt change bHasProblem with work functions because it has to happen only when widget animation is done

	UFUNCTION(BlueprintCallable)
	void ProblemArisen();
	//void ProblemArisen_Implementation();

	UFUNCTION(BlueprintCallable)
	void ProblemCleared();
	//void ProblemCleared_Implementation();



	/**/
};
