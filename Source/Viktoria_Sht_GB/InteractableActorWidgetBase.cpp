// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractableActorWidgetBase.h"
#include "InteractableActor.h"
#include "Components/TextBlock.h"
#include "Components/WrapBox.h"
#include "Components/Border.h"


//Setting up new money value in ObjectMoneyText
void UInteractableActorWidgetBase::SetMoney(int32 CurrentMoney)
{
	if (ObjectMoneyText)
	{
		ObjectMoneyText->SetText(FText::AsNumber(CurrentMoney));
	}
}

//Setting up name and description
void UInteractableActorWidgetBase::SetNameDesc(FString _Name, FText _Description)
{
	Name->SetText(FText::FromString(_Name));
	Description->SetText(_Description);
}

void UInteractableActorWidgetBase::UpdatePowerUpCost(int32 PowerUpCost)
{
	PowerUpCostText->SetText(FText::AsNumber(PowerUpCost));
}

void UInteractableActorWidgetBase::CantPowerUp()
{
	PowerUpTipBorder->RemoveFromParent();
	PowerUpCostText->SetText(FText::FromString("MAX Level!"));
}

void UInteractableActorWidgetBase::MakeNameDescVisible_Implementation()
{
}

void UInteractableActorWidgetBase::MakeNameDescNotVisible_Implementation()
{
}

void UInteractableActorWidgetBase::StartDoingWork_Implementation()
{
}

void UInteractableActorWidgetBase::EndDoingWork_Implementation()
{
}







